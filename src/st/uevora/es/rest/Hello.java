package st.uevora.es.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

//Sets the path to base URL + /hello
@Path("/hello")
public class Hello {

	@GET
	@Path("/world")
	public String hello() {
		return "Hello World";		
	}
		
	@GET
	@Path("/world1")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hello1() {
		JSONObject response = new JSONObject();
		response.put("message", "helloWorld");

		return Response
				.status(200)
				.entity(response.toString()).build();
		
	}

	@GET
	@Path("/name")
	@Produces(MediaType.APPLICATION_JSON)
	public Response helloName(
			@QueryParam("name") String name,
			@QueryParam("country") String country) {

		JSONObject response = new JSONObject();
		response.put("message", "Hello " + name + " from " + country);
		
		return Response
				.status(200)
				.entity(response.toString()).build();
	}

	@POST
	@Path("/name")
	@Produces(MediaType.APPLICATION_JSON)
	public Response helloName1(String requestBody){
		JSONObject obj = new JSONObject(requestBody);
		String name=obj.getString("name");
		String country=obj.getString("country");
		
		JSONObject response = new JSONObject();
		response.put("message", "Hello " + name + " from " + country);
		
		return Response
				.status(200)
				.entity(response.toString()).build();		
	}

} 