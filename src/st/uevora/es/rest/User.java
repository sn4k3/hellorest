package st.uevora.es.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

//Sets the path to base URL + /user
@Path("/user")
public class User {

	@GET
	@Path("/world")
	public String hello() {
		return "Hello World";		
	}
	
	@POST
	@Path("/info")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response teste(String requestBody){

		JSONObject obj = new JSONObject(requestBody);
		String name=obj.getString("name");
		String address=obj.getString("address");
		String email=obj.getString("email");
		JSONArray phonesArray=obj.getJSONArray("phones");
		
		String phonesString = "";
		for (int i = 0; i < phonesArray.length() - 1; i++) {
			  phonesString += phonesArray.get(i).toString();
			  phonesString += ", ";
		}	
		phonesString += phonesArray.get(phonesArray.length() - 1).toString();
		
		String message = "Welcome " + name + ", your phone numbers are: " + phonesString;
		
		JSONObject response = new JSONObject();
		response.put("message", message);
				
		return Response
				.status(200)
				.entity(response.toString()).build();
	}

} 